
require('dotenv').config();
// Generate Token Using Secret from process.env.JWT_SECRET
var jwt = require('jsonwebtoken')


// Generate Token and return item
function generateToken(user) {
    // 1. Don't user password and other sensitive fields
    // 2. Use the information that are usefull in other parts

    if (!user) return null

    var uData = {
        userid: user.id_user,
        name: user.name,
        email: user.email, 
        isAdmin: user.role
    };


    return jwt.sign(uData, process.env.JWT_SECRET, {
        expiresIn: 60 * 60 * 24 // Expires token in 24 hours
    });
}

// return basic user details
function getCleanUser(user) {
    if (!user) return null;

    return {
        userid: user.id_user,
        name: user.name,
        email: user.email, 
        isAdmin: user.role
    }
}


module.exports = {
    generateToken,
    getCleanUser
}