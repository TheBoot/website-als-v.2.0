require('dotenv').config();

const path = require('path');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

var db = require('./db')

const ENV = process.env.NODE_ENV;
const PORT = process.env.PORT || 5000;

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(bodyParser.json());


// End Point API
app.use('/api/users', require('./api/userapi'));
app.use('/api/login', require('./api/loginapi'));
app.use('/api/categories', require('./api/categoryapi'));
app.use('/api/products', require('./api/productapi'));
app.use('/api/images', require('./api/imageapi'));
app.use('/api/chart',require('./api/chartapi'));
app.use('/api/transaction', require('./api/transactionapi'));


app.listen(PORT, () => {
    console.log(`webserver al's listening on port ${PORT}..`);
});

// Connection to database
db.query('SELECT NOW()', (err,res) => {
    if(err.error) {
        return console.log(err.error);
    }
    console.log(`PostgreSQL connection established : ${res[0].now}.`);
});


module.exports = app;