const db = require('../db')

class Transaction {

    static retreiveAll(callback) {
        db.query('SELECT * FROM t_transaction', (err, result) => {
            if (err.error){
                return callback(err)
            }
            calback(result)
        })
    }

    static retreivebyId(transactionId, callback) {
        db.query('SELECT * FROM t_transaction where id_transaction = $1', [transactionId], (err, result) => {
            if (err.error) {
                return callback(err)
            }
            callback(result)
        })
    }

    static insert(transaction, callback) {
        db.query('INSERT INTO t_transaction (id_transaction, id_product, id_user, quantity, created_by, created_date VALUES ($1, $2, $3, $4, $5, current_timestamp)', [transaction.id_transaction, transaction.id_product, transaction.id_user, transaction.quantity, transaction.created_by], (err, result) =>{
            if (err.error){
                return callback(err);
            }
            callback(result);
        })
    }

    static update(param_id, transaction, callback) {
        db.query('UPDATE t_transaction SET id_product = $1, id_user = $2, quantity = $3, modified_by=$4, modified_date=current_timestamp WHERE id_transaction=$5', [transaction.id_product, transaction.id_user, transaction.quantity, transaction.modified_by, param_id], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static delete(transaction, callback){
        db.query('DELETE FROM t_transaction where id_chart=$1',[transaction.id_transaction], (err, result) => {
            if(err.error){
                return callback(err);
            }
            callback(result);
        });
    }

}

module.exports = Transaction;