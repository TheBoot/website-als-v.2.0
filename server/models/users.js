const db = require('../db');

class Users {

    static retreiveAll(callback) {
        db.query('SELECT * FROM t_user', (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        })
    }

    static retreive(userdata, callback) {
        db.query('SELECT * FROM t_user WHERE id_user = $1', [userdata], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        })
        
    }

    static insert(userdata, callback) {
        db.query('INSERT INTO t_user (id_user, password, name, email, address, phone, gender, role, created_by, created_date) VALUES ($1, md5($2), $3, $4, $5, $6, $7, $8, $9, current_timestamp)', [userdata.id_user, userdata.password, userdata.name, userdata.email, userdata.address, userdata.phone, userdata.gender, userdata.role, userdata.created_by], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        })
    }

    static update(param_id, userdata, callback) {
        db.query('UPDATE t_user SET name = $1, password = md5($2), email = $3, address = $4, phone = $5, gender = $6, role = $7, modified_by=$8, modified_date=current_timestamp WHERE id_user=$9', [userdata.name, userdata.password, userdata.email, userdata.address, userdata.phone, userdata.gender, userdata.role, userdata.modified_by, param_id], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static delete(userdata, callback) {
        db.query('DELETE FROM t_user where id_user=$1', [userdata.id_user], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }


}

module.exports = Users;