const db = require('../db');

class Images {

    static retreiveAll(callback) {
        db.query('SELECT * FROM t_images', (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static retreiveAllbyProduct(images, callback) {
        db.query('SELECT * FROM t_images WHERE id_product = $1', [images], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static retreive(images, callback) {
        db.query('SELECT * FROM t_images WHERE id_image = $1', [images], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        })

    }


    static insert(images, callback) {
        db.query('INSERT INTO t_images (id_image, name, location_image, id_product, created_by, created_date) VALUES ($1, $2, $3, $4, $5, current_timestamp)', [images.id_image, images.name, images.location_image, images.id_product, images.created_by], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static update(param_id, images, callback) {
        db.query('UPDATE t_images SET name=$1, id_product=$2, modified_by=$3, modified_date=current_timestamp WHERE id_image=$4', [images.name, images.id_product, images.modified_by, param_id], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static delete(images, callback) {
        db.query('DELETE FROM t_images where id_image=$1', [images.id_image], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

}

module.exports = Images;