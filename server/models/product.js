const db = require('../db');

class Products {

    static retreiveAll(callback) {
        db.query('SELECT * FROM t_product', (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static retreiveAllbyCategory(product, callback) {
        db.query('SELECT * FROM t_product WHERE id_category = $1',[product], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static retreive(product, callback) {
        db.query('SELECT * FROM t_product WHERE id_product = $1', [product], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        })

    }
    

    static insert(product, callback) {
        db.query('INSERT INTO t_product (id_product, name, id_category, description, link_product, price, size, stock, created_by, created_date) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, current_timestamp)', [product.id_product, product.name, product.id_category, product.description, product.link_product, product.price, product.size, product.stock, product.created_by], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static update(param_id, product, callback) {
        db.query('UPDATE t_product SET name=$1, id_category=$2, description=$3, link_product=$4, price=$5, size=$6, stock=$7, modified_by=$8, modified_date=current_timestamp WHERE id_product=$9', [product.name, product.id_category, product.description, product.link_product, product.price, product.size, product.stock, product.modified_by, param_id], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static updatestock(param_id, product_stock) {
        db.query('UPDATE t_product SET stock=$1, modified_by=$2, modified_date=current_timestamp WHERE id_product=$3', [product_stock, product.modified_by, param_id], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static delete(product, callback) {
        db.query('DELETE FROM t_product where id_product=$1', [product.id_product], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

}

module.exports = Products;