const db = require('../db');

class Categories {

    static retreiveAll(callback) {
        db.query('SELECT * FROM t_categories', (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static retreive(category, callback) {
        db.query('SELECT * FROM t_categories WHERE id_category = $1', [category], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        })
        
    }

    static insert(category, callback) {
        db.query('INSERT INTO t_categories (id_category, name, created_by, created_date) VALUES ($1, $2, $3, current_timestamp)', [category.id_category, category.name, category.created_by], (err, result) => {
            if(err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static update(param_id, category, callback) {
        db.query('UPDATE t_categories SET name=$1, modified_by=$2, modified_date=current_timestamp WHERE id_category=$3',[category.name,category.modified_by,param_id], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static delete(category, callback) {
        db.query('DELETE FROM t_categories where id_category=$1', [category.id_category], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

}

module.exports = Categories;