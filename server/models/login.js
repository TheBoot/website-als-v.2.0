const db = require('../db');

class Login {

    static logincheck(userdata, callback) {
        db.query('SELECT * FROM t_user WHERE id_user = $1 LIMIT 1', [userdata], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        })
        
    }

}

module.exports = Login;