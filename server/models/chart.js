const db = require('../db')

class Chart {

    static retreiveAll(callback) {
        db.query('SELECT * FROM t_chart', (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result)
        });
    }

    static retreive(chartdata, callback) {
        db.query('SELECT * FROM t_chart WHERE id_chart = $1', [chartdata], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static retreiveByUserId(chartdata, callback) {
        db.query('SELECT * FROM t_chart WHERE id_user = $1', [chartdata], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static insert(chartdata, callback) {
        db.query('INSERT INTO t_chart (id_chart, id_product, id_user, quantity, created_by, created_date) VALUES ($1, $2, $3, $4, $5, current_timestamp)', [chartdata.id_chart, chartdata.id_product, chartdata.id_user, chartdata.id_quantity, chartdata.created_by], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static update(param_id, chartdata, callback) {
        db.query('UPDATE t_chart SET id_product = $1, id_user = $2, quantity = $3, modified_by=$4, modified_date=current_timestamp WHERE id_chart=$5', [chartdata.id_product, chartdata.id_user, chartdata.quantity, chartdata.modified_by, param_id], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

    static delete(chartdata, callback) {
        db.query('DELETE FROM t_chart where id_chart=$1', [chartdata.id_chart], (err, result) => {
            if (err.error) {
                return callback(err);
            }
            callback(result);
        });
    }

}

module.exports = Chart;