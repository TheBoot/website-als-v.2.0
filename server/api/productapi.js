var express = require('express');
var Products = require('../models/product');

var router = express.Router();

router.get('/', (req, res) => {
   
    let category = req.query.category;

    if (category === undefined) {
        // Get All Products
        Products.retreiveAll((err, result) => {
            if (err) {
                return res.json(err);
            }
            return res.json(result);
        });
    } else {
        // Get All Products by Categories
        Products.retreiveAllbyCategory(category, (err, result) => {
            if (err) {
                return res.json(err);
            }
            return res.json(result);
        });
    }
});


router.get('/:id', (req, res) => {
    let params = req.params.id;
    Products.retreive(params, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.post('/', (req, res) => {
    let product = req.body;

    Products.insert(product, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.put('/:id', (req, res) => {
    let product = req.body;
    let param_id = req.params.id;

    console.log("user: " + product);
    console.log("params : " + param_id);

    Products.update(param_id, product, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.delete('/', (req, res) => {
    let product = req.body;

    Products.delete(product, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});

module.exports = router;