var express = require('express');
var Transactions = require('../models/transaction');
var Chart = require('../models/chart');
var Product = require('../models/product');


var router = express.Router();

router.get('/', (req, res) => {

    Transactions.retreiveAll((err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});

router.get('/:id', (req, res) => {
    let params = req.params.id;
    console.log(params);
    Transactions.retreive(params, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.post('/', (req, res) => {
    let transactiondata = req.body;

    console.log(transactiondata);
    // Before Insert Data Check on Chart User
    // Code bellow
    Chart.retreiveByUserId(transactiondata.id_chart, (err, result) => {
        if (err) {
            return res.json(err);
        }

        // Set State transaction to "process"
        Transactions.insert(transactiondata, (err, result) => {
            if (err) {
                return res.json(err);
            }
            return res.json(result);
        });
    });

    // Delete Chart Data
    Chart.delete(transaction.id_chart, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });

});


// Admin update transaction state to "finisihed"
router.put('/:id', (req, res) => {
    let transactiondata = req.body;
    let param_id = req.params.id;

    console.log("transactiondata: " + transactiondata);
    console.log("params : " + param_id);

    Transactions.update(param_id, transactiondata, (err, result) => {
        if (err) {
            return res.json(err);
        }
        // update stock on t_product
        // Code in bellow
        Product.retreive(transactiondata.id_product, (err, resultproduct) => {
            if (err) {
                return res.json(err);
            }
            stock = result.stock - transactiondata.quantity;
            Product.updatestock(result.id_product, stock, (err, resultstock) => {
                if (err) {
                    return res.json(err);
                }
            })
        })
        // -------------------------------- //
        return res.json(result);
    });

});


router.delete('/', (req, res) => {
    let transactiondata = req.body;

    Transactions.delete(transactiondata, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });

});

module.exports = router;