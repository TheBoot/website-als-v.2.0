var express = require('express');
var Images = require('../models/image');

var router = express.Router();

router.get('/', (req, res) => {
   
    let product = req.query.product;
    console.log(product);

    if (product === undefined) {
        // Get All Images
        Images.retreiveAll((err, result) => {
            if (err) {
                return res.json(err);
            }
            return res.json(result);
        });
    } else {
        // Get All Images by Categories
        Images.retreiveAllbyProduct(product, (err, result) => {
            if (err) {
                return res.json(err);
            }
            return res.json(result);
        });
    }
});


router.get('/:id', (req, res) => {
    let params = req.params.id;
    Images.retreive(params, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.post('/', (req, res) => {
    let image = req.body;
    
    // Save Image to File
    // Code Here: 


    // Insert Database if save file success
    Images.insert(image, (err, result) => {
        if (err) {
            // delete file images

            // response to frontend
            return res.json(err);
        }
        return res.json(result);
    });
});


router.put('/:id', (req, res) => {
    let image = req.body;
    let param_id = req.params.id;

    console.log("images: " + image);
    console.log("params : " + param_id);

    Images.update(param_id, image, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.delete('/', (req, res) => {
    let image = req.body;

    Images.delete(image, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});

module.exports = router;