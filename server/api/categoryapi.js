var express = require('express');
var Categories = require('../models/category');

var router = express.Router();

router.get('/', (req, res) => {
    Categories.retreiveAll((err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.get('/:id', (req, res) => {
    let params = req.params.id;
    Categories.retreive(params, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.post('/', (req, res) => {
    let category = req.body;

    Categories.insert(category, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});

router.put('/:id', (req, res) => {
    let category = req.body;
    let param_id = req.params.id;

    console.log("user: " + category);
    console.log("params : " + param_id);

    Categories.update(param_id, category, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.delete('/', (req, res) => {
    let category = req.body;

    Categories.delete(category, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});

module.exports = router;