var express = require('express');
var Chart = require('../models/chart');
var Product = require('../models/product');

var router = express.Router();

router.get('/', (req, res) => {

    Chart.retreiveAll((err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});

router.get('/:id', (req, res) => {
    let params = req.params.id;
    console.log(params);
    Chart.retreiveByUserId(params, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});

router.get('/:id/:id_chart', (req, res) => {
    let params = req.params.id_chart;
    console.log(params);
    Chart.retreive(params, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.post('/', (req, res) => {
    let chartdata = req.body;

    console.log(chartdata);
    // Before Insert, Check Data Stock on Id Product
    // Code bellow
    Product.retreive(chartdata.id_product, (err, result) => {
        if (err) {
            return res.json(err);
        }

        qty = json(result)
        if (qty.quantity != 0) {
            Chart.insert(chartdata, (err, result) => {
                if (err) {
                    return res.json(err);
                }
                return res.json(result);
            });
        } else {
            return res('{error : out of stock}');
        }

    });

});

router.put('/:id', (req, res) => {
    let chartdata = req.body;
    let param_id = req.params.id;

    console.log("chartdata: " + chartdata);
    console.log("params : " + param_id);

    Chart.update(param_id, chartdata, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });

});


router.delete('/', (req, res) => {
    let chartdata = req.body;

    Chart.delete(chartdata, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });

});

module.exports = router;