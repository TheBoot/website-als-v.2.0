var express = require('express');
const session = require('express-session');
const bcrypt = require('bcrypt');
var md5 = require('md5');
var Login = require('../models/login');

const utils = require('../utils')
const jwt = require('jsonwebtoken')

var router = express.Router();
router.use((req, res, next) => {
    var token = req.headers['authorization'];
    // if no token, continue 
    if(!token) return next(); 

    token = token.replace('Bearer ', '');
    jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
        if (err) {
            return res.status(401).json({
                error: true,
                message: 'Invalid User'
            })
        }
        else {
            req.user = user;
            next();
        }
    })
})

router.post('/', (req, res) => {
    let username = req.body.username;
    let password = md5(req.body.password);

    if (username.length > 32 || password.length > 32) {
        return res.status(400).json({
            success: false,
            message: 'An error occured, please try again'
        });
    }

    // return 400 status if username/password is not exist
    if (!username || !password) {
        return res.status(400).json({
            error: true,
            message: "Username or Password required."
        });
    }


    Login.logincheck(username, result => {
        if (result && result.length === 1) {
            // bcrypt.compare(password, result[0].password, (bcryptErr, verified) =>{

            if (password === result[0].password) {

                // req.session.UserId = result[0].id_user;
                // console.log(req.session)

                // Generate Token from userdata
                const token = utils.generateToken(result[0])

                // Get Basic User details
                const userObj = utils.getCleanUser(result[0])

                // Return the token along with user details
                return res.status(200).json({
                    success: true,
                    user: userObj, token
                })
            }
            else {
                return res.status(401).json({
                    success: false,
                    message: "Invalid Password"
                })
            }
            // })

        } else {
            return res.status(400).json({
                success: false,
                message: "User Not Found"
            })
        }
    });
});


router.post('/logout', (req, res) => {

    if (req.session.UserId) {
        req.session.destroy();
        return res.json({
            success: true
        })
    } else {
        return res.json({
            success: false
        })
    }
});

// Verify the token
router.get('/verifyToken', (req, res) => {
    // header check or url parameters or post parameters for token validation
    var token = req.body.token || req.query.token;
    if (!token) {
        return res.status(400).json({
            error: true,
            message: 'Token is Required.'
        })
    }

    // Check token that wass passed by decoding token using secret
    jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
        if (err) {
            return res.status(401).json({
                error: true,
                message: 'Invalid Token.'
            });
        }
        
        let userid = user.userid;
        console.log(userid)
        Login.logincheck(userid, result => {
            if (result && result.length === 1) {
                if (userid === result[0].id_user) {
                    // get basic user details
                    var userObj = utils.getCleanUser(result[0]);;
                    return res.status(200).json({
                        success: true,
                        user: userObj, token
                    })
                }
                else {
                    return res.status(401).json({
                        success: false,
                        message: "Invalid Password"
                    })
                }
            } else {
                return res.status(400).json({
                    success: false,
                    message: "User Not Found"
                })
            }
        })



    });


})


router.post('/isloggedin', (req, res) => {

    if (req.session.UserId) {
        Login.logincheck(req.session.UserId, result => {
            if (result && result.length === 1) {
                return res.json({
                    success: true,
                    username: result[0].name
                })
            }
            else {
                return res.json({
                    success: false
                })
            }
        });
    } else {
        return res.json({
            success: false
        })
    }
});



module.exports = router;