var express = require('express');
var Users = require('../models/users');

var router = express.Router();

router.get('/', (req, res) => {

    Users.retreiveAll((err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});

router.get('/:id', (req, res) => {
    let params = req.params.id;
    console.log(params);
    Users.retreive(params, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });
});


router.post('/', (req, res) => {
    let user = req.body;

    console.log(user);

    Users.insert(user, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });


});

router.put('/:id', (req, res) => {
    let user = req.body;
    let param_id = req.params.id;

    console.log("user: " + user);
    console.log("params : " + param_id);

    Users.update(param_id, user, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });

});


router.delete('/', (req, res) => {
    let user = req.body;

    Users.delete(user, (err, result) => {
        if (err) {
            return res.json(err);
        }
        return res.json(result);
    });

});

module.exports = router;