var express = require('express');
const session = require('express-session');
const bcrypt = require('bcrypt');
var md5 = require('md5');
var Login = require('../models/login');

var router = express.Router();
router.use(session({
    key: 'jsacndmswd13813zxfcl13qw11',
    secret: 'aksnd13emsaew011231tfasd11',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: (3600 * 60),
        httpOnly: false
    }
}));

router.post('/', (req, res) => {
    let username = req.body.username;
    let password = md5(req.body.password);

    if (username.length > 32 || password.length > 32) {
        return res.json({
            success: false,
            message: 'An error occured, please try again'
        });
    }

    Login.logincheck(username, result => {


        if (result && result.length === 1) {
            // bcrypt.compare(password, result[0].password, (bcryptErr, verified) =>{
        
            if (password === result[0].password) {
               
                req.session.UserId = result[0].id_user;
                // console.log(req.session)
                return res.json({
                    success: true,
                    username: result[0].name
                })
            }
            else {
                return res.json({
                    success: false,
                    message: "Invalid Password"
                })
            }
            // })

        } else {
            return res.json({
                success: false,
                message: "User Not Found"
            })
        }
    });
});


router.post('/logout', (req, res) => {

    if (req.session.UserId) {
        req.session.destroy();
        return res.json({
            success: true
        })
    } else {
        return res.json({
            success: false
        })
    }
});

router.post('/isloggedin', (req, res) => {

    if (req.session.UserId) {
        Login.logincheck(req.session.UserId, result => {
            if (result && result.length === 1) {
                return res.json({
                    success: true,
                    username: result[0].name
                })
            }
            else {
                return res.json({
                    success: false
                })
            }
        });
    } else {
        return res.json({
            success: false
        })
    }
});



module.exports = router;