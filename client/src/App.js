import React, { Component} from 'react'
import { BrowserRouter, Route, Switch} from 'react-router-dom'

import Home from './landingpage/home.js'
import Product from './components/products/product.js'
import About from './components/about.js'
import NavbarAls from './components/Utils/navbarals.js'
import FooterAls from './components/Utils/footer.js'
import Login from './components/Login/login.js'
import Dashboard from './components/dashboard.js'
import UserList from './components/Users/userlist.js'
import UserRegister from './components/Users/userregister'
import ProductManagement from './components/products/productManagement'
import TransactionManagement from './components/transactions/transactionsManagement'

import PublicRoute from './components/Utils/publicRoute';
import PrivateRoute from './components/Utils/privateRoute';
import { getToken, setUserSession } from './components/Utils/common'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      authLoading: true,
      token: null
    }
  }

  componentDidMount() {
    let tokendata = getToken();
    this.setState({ token: tokendata });
  }

  render() {
    // Handle Refresh Page - Keep User Logged In
    if (this.state.authLoading && this.state.token != null) {
      fetch(`/api/login/verifyToken?token=${this.state.token}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }

      })
        .then((res) => res.json())
        .then((result) => {
          this.setState({ authLoading: false });
          if (!result.success) {
            console.log(result.message)
          } else {
            setUserSession(result.token, result.user);
          }
          
        })

      return <div className="content">Checking Authentication...</div>
    } else {
      return (
        <BrowserRouter>
          <div className="container-fluid bg-dark">
            <div className="header">
              <NavbarAls />
            </div>
            <div className='content container'>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path='/product' component={Product} />
                <Route path='/about' component={About} />
                <PublicRoute exact path='/login' component={Login} />
                <PrivateRoute exact path="/dashboard" component={Dashboard} />
                <PrivateRoute path='/user-management' component={UserList} />
                <PrivateRoute path='/product-management' component={ProductManagement} />
                <PrivateRoute path='/transactions' component={TransactionManagement} />
                <PrivateRoute path='/user-register' component={UserRegister} />
                {/* <Route exact path="/login" render={() => <Login state={this.state.Username} isloggedin={this.state.isloggedin} />} /> */}
                {/* <Route path='/product' render={(props)=> <Product {...props} login={this.state.login} />} /> */}
                {/* <Route path='/product' render={() => (
                this.state.login ? <Product /> : (<Redirect to="/" />)
              )} /> */}

              </Switch>
            </div>
            <div className='footer'>
              <FooterAls />
            </div>
          </div>
        </BrowserRouter>
      )
    }

  }
}

export default App;

