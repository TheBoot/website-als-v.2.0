import React from 'react'
// import UsersHandler from './usershandler'

// import ImageHandler from './imageshandler.js'

const Home = () => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-lg-6 col-md-12 caption-two-panel ml-auto pt-5">
                    <img src={process.env.PUBLIC_URL + '/img/IMG_3058_als.jpg'} width="100%" height="300" alt="item"></img>
                </div>
                <div className="col-lg-6 col-md-12 caption-two-panel ml-auto pt-5">
                    <div className="intro-caption mt-5">
                        <h1 className="text-white mb-2">It's not really hard to stand out in crowd</h1>
                        <p className="text-white mb-4"> These are the voyages of the Starship Enterprise. Its five-year mission: to explore strange new worlds, to seek out new life and new civilizations, to boldly go where no man has gone before.</p>
                        <a href="/products" className="btn btn-primary text-white mr-3">Explore More</a>
                    </div>
                </div>
            </div>
            <br />


            <div className="row">
                <div className="col-md-4 col-sm-6">
                    <div className="product-grid8">
                        <div className="product-image8">
                            <a href="/product/wallet">
                                <img className="pic-1" src={process.env.PUBLIC_URL + '/img/IMG_3058_als.jpg'} alt="product walet"></img>
                                <img className="pic-2" src={process.env.PUBLIC_URL + '/img/IMG_3058_als.jpg'} alt="product walet"></img>
                            </a>
                            <ul className="social">
                                <li><a href="/product/wallet/search?id=91cas1" className="fa fa-search"> </a></li>
                                <li><a href="/product/wallet?id=91cas1" className="fa fa-shopping-bag"> </a></li>
                                <li><a href="/product/chart/wallet?id=91cas1" className="fa fa-shopping-cart"> </a></li>
                            </ul>
                            <span className="product-discount-label">-20%</span>
                        </div>
                        <div className="product-content">
                            <div className="price">£ 8.00
                        <span>£ 10.00</span>
                            </div>
                            <span className="product-shipping">Free Shipping</span>
                            <h3 className="title"><a href="/product/wallet">Women's Plain Top</a></h3>
                            <a className="all-deals" href="/product">See all deals <i className="fa fa-angle-right icon"></i></a>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 col-sm-6">
                    <div className="product-grid8">
                        <div className="product-image8">
                            <a href="/product/straps">
                                <img className="pic-1" src={process.env.PUBLIC_URL + '/img/IMG_3058_als.jpg'} alt="product straps"></img>
                                <img className="pic-2" src={process.env.PUBLIC_URL + '/img/IMG_3058_als.jpg'} alt="product straps"></img>
                            </a>
                            <ul className="social">
                                <li><a href="/product/straps/search?id=91cas1" className="fa fa-search"> </a></li>
                                <li><a href="/product/straps?id=91cas1" className="fa fa-shopping-bag"> </a></li>
                                <li><a href="/product/chart/straps?id=91cas1" className="fa fa-shopping-cart"> </a></li>
                            </ul>
                            <span className="product-discount-label">-30%</span>
                        </div>
                        <div className="product-content">
                            <div className="price">£ 7.00
                            <span>£ 10.00</span>
                            </div>
                            <span className="product-shipping">Free Shipping</span>
                            <h3 className="title"><a href="/product/straps">Women's Designer Top</a></h3>
                            <a className="all-deals" href="/product">See all deals <i className="fa fa-angle-right icon"></i></a>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 col-sm-6">
                    <div className="product-grid8">
                        <div className="product-image8">
                            <a href="/product/bag">
                                <img className="pic-1" src={process.env.PUBLIC_URL + '/img/IMG_3058_als.jpg'} alt="product bag"></img>
                                <img className="pic-2" src={process.env.PUBLIC_URL + '/img/IMG_3058_als.jpg'} alt="product bag"></img>
                            </a>
                            <ul className="social">
                                <li><a href="/product/bag/search?id=91cas1" className="fa fa-search"> </a></li>
                                <li><a href="/product/bag?id=91cas1" className="fa fa-shopping-bag"> </a></li>
                                <li><a href="/product/chart/bag?id=91cas1" className="fa fa-shopping-cart"> </a></li>
                            </ul>
                            <span className="product-discount-label">-20%</span>
                        </div>
                        <div className="product-content">
                            <div className="price">£ 8.00
                        <span>£ 10.00</span>
                            </div>
                            <span className="product-shipping">Free Shipping</span>
                            <h3 className="title"><a href="/product/bag">Women's Plain Top</a></h3>
                            <a className="all-deals" href="/product">See all deals <i className="fa fa-angle-right icon"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Home