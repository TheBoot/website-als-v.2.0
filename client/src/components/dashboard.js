import React from 'react'
import { Route, Redirect, NavLink } from 'react-router-dom';
import { getUser, removeUserSession } from './Utils/common' 


const Dashboard = (props) => {
    const user = getUser();

    const handleLogout = () => {
        removeUserSession();
        props.history.push('/login');
    }

    try {
        return (
                <div className="container">
                    <hr className="mt-3 mb-3 border-white" />
                    <h4 className="text-white">DASHBOARD</h4>
                    <h1 className="text-white">Welcome, {user.name}</h1>
                    <hr className="mt-3 mb-3 border-white" />
                    <input type="button" onClick={handleLogout} value="Logout" />
                    <div className="row mt-2 mb-2">
                        <div className="col text-center mt-2 mb-2 ">
                            <div className="card" >
                                <img className="card-img-top" src="https://source.unsplash.com/random" alt="User Management"></img>
                                <div className="card-body">
                                    <h5 className="card-title">User Management</h5>
                                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <NavLink to="/user-management" className="btn btn-primary">Go User Management</NavLink>
                                </div>
                            </div>
                        </div>
                        <div className="col text-center mt-2 mb-2">
                            <div className="card" styleName="width: 18rem;">
                                <img className="card-img-top" src="https://source.unsplash.com/random" alt="Product Management"></img>
                                <div className="card-body">
                                    <h5 className="card-title">Product Management</h5>
                                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <NavLink to="/product-management" className="btn btn-primary">Go Product Management</NavLink>
                                </div>
                            </div>
                        </div>
                        <div className="col text-center mt-2 mb-2">
                            <div className="card" styleName="width: 18rem;">
                                <img className="card-img-top" src="https://source.unsplash.com/random" alt="Transaction Management"></img>
                                <div className="card-body">
                                    <h5 className="card-title">Transaction Management</h5>
                                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <NavLink to="/transactions" className="btn btn-primary">Go transaction</NavLink>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            )
    } catch (error) {
        return (
            <div>
                <Route path='/dashboard' render={() => (<Redirect to="/login" />)} />
            </div>
        )
    }


}

export default Dashboard