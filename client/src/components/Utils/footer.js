import React from 'react'
import { NavLink } from 'react-router-dom';
import {
    Card,
    CardBody
   
} from 'reactstrap';

const FooterAls = () => {
    return (
            <Card className="text-center bg-dark">
                <CardBody>
                    <h4 className="card-title text-white">
                        Animal Leather Store
                </h4>
                    <ul className="list-inline">
                        <li className="list-inline-item">
                            <NavLink exact to="/" className="nav-link ">Home</NavLink>
                        </li>
                        <li className="list-inline-item">
                            <NavLink to="/product" className="nav-link ">Product</NavLink>
                        </li>
                        <li className="list-inline-item">
                            <NavLink to="/about" className="nav-link ">About</NavLink>
                        </li>
                        <li className="list-inline-item">
                            <NavLink to="/login" className="nav-link ">Login</NavLink>
                        </li>

                    </ul>
                </CardBody>
            </Card>
    )

    //.card.text-center.bg-dark
    //     .card-body(style='color:#D4AF37')
    //     h4.card-title Animal Leather Straps
    //     ul.list-inline
    //         li.list-inline-item
    //           a.footer-link(href='/product' style='color:#D4AF37') Product
    //         li.list-inline-item
    //           a.footer-link(href='/contact' style='color:#D4AF37') Contact

    //   .card-footer.text-muted
    //     div#footer-copyright © strapkulit.com. Hak Cipta Dilindungi

}

export default FooterAls