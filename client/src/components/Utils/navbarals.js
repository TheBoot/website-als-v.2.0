import React from 'react'
import { Link, NavLink } from 'react-router-dom';


const NavbarAls = () => {
    return (
        <div className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container">
                <Link to='/' className="navbar-brand">Animal Leather Store</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul className="nav navbar-nav">
                        <li className="nav-item">
                            <NavLink exact to="/" className="nav-link ">Home</NavLink>
                        </li>
                        <li className="nav-item ">
                            <NavLink to="/product" className="nav-link ">Product</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/about" className="nav-link ">About</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/login" className="nav-link ">Login</NavLink>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    )
}

export default NavbarAls