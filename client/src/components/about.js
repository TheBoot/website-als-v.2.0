import React from 'react'

const About = () => {
    return (
        <div className="container bootstrap snippets text-white">
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-7 col-lg-7 pull-right" styleName="z-index:1020;">
                    <div id="carousel-example-generic" className="carousel slide">
                        <ol className="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" className=""></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1" className="active"></li>
                        </ol>
                        <div className="carousel-inner">
                            <div className="item">
                                <img src="https://via.placeholder.com/650x300/" alt="item"></img>
                                <div className="carousel-caption">
                                    <p>Item 1</p>
                                </div>
                            </div>
                            {/* <div className="item active">
                                <img src="https://via.placeholder.com/650x300/" alt="item"></img>
                                <div className="carousel-caption">
                                    <p>Item 2</p>
                                </div>
                            </div> */}
                        </div>
                        <a className="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span className="icon-prev"></span>
                        </a>
                        <a className="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span className="icon-next"></span>
                        </a>
                    </div>
                    <hr></hr>
                </div>

                <div className="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <div className="well">
                        <h2>ABOUT ANIMAL LEATHER STORE (AL'S) </h2>
                        <p className="">Here goes sub-title. Write few words about your awesome project</p>
                    </div>
                    <p>
                        Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
      </p>
                </div>
            </div>
            <div className="row text-center">
                <div className="col-sm-4">
                    <div className="contact-detail-box">
                        <i className="fa fa-th fa-3x text-colored"></i>
                        <h4>Get In Touch</h4>
                        <abbr title="Phone">Phone : </abbr>
                            +62 812-8913-1832
                            <br></br>
                            Email : <a href="mailto:animal.leathestrap@gmail.com" className="text-muted">
                            animal.leathestrap@gmail.com
                                </a>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="contact-detail-box">
                        <i className="fa fa-map-marker fa-3x text-colored"></i>
                        <h4>Our Location</h4>

                        <address>
                            Ngaglik, Sleman<br></br>
                            Special Region of Yogyakarta, Indonesia<br></br>
                        </address>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="contact-detail-box">
                        <i className="fa fa-book fa-3x text-colored"></i>
                        <h4>24x3 Process</h4>
                        <p>Handmade industry's standard.</p>
                        {/* <h4 className="text-muted">1234 567 890</h4> */}
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-sm-6">
                    <div className="contact-map">
                        <iframe title="Workshop Animal Leather Store - Al's" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.7162011942623!2d110.37754751538182!3d-7.713570278557111!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a59638dd98351%3A0x68e9efb53b07b5e5!2sAnimal%20Leather%20Store!5e0!3m2!1sid!2sid!4v1594827283016!5m2!1sid!2sid" width="100%" height="360" frameborder="0" styleName="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
                {/* Contact Form */}
                <div className="col-sm-6">
                    <form name="ajax-form" id="ajax-form" action="https://formsubmit.io/send/coderthemes@gmail.com" method="post" className="form-main">
                        <div className="form-group">
                            <label for="name2">Name</label>
                            <input className="form-control" id="name2" name="name" onblur="if(this.value == '') this.value='Name'" onfocus="if(this.value == 'Name') this.value=''" type="text" value="Name"></input>
                            <div className="error" id="err-name" styleName="display: none;">Please enter name</div>
                        </div>
                        <div className="form-group">
                            <label for="email2">Email</label>
                            <input className="form-control" id="email2" name="email" type="text" onfocus="if(this.value == 'E-mail') this.value='';" onblur="if(this.value == '') this.value='E-mail';" value="E-mail"></input>
                            <div className="error" id="err-emailvld" styleName="display: none;">E-mail is not a valid format</div>
                        </div>
                        <div className="form-group">
                            <label for="message2">Message</label>
                            <textarea className="form-control" id="message2" name="message" rows="5" onblur="if(this.value == '') this.value='Message'" onfocus="if(this.value == 'Message') this.value=''">Message</textarea>
                            <div className="error" id="err-message" styleName="display: none;">Please enter message</div>
                        </div>
                        <div className="row">
                            <div className="col-sm-4">
                                <div id="ajaxsuccess" className="text-success">E-mail was successfully sent.</div>
                                <div className="error" id="err-form" styleName="display: none;">There was a problem validating the form please check!</div>
                                <div className="error" id="err-timedout">The connection to the server timed out!</div>
                                <div className="error" id="err-state"></div>
                                <button type="submit" className="btn btn-primary btn-shadow btn-rounded w-md" id="send">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <br />
        </div>
    )
}

export default About