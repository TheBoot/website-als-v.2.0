import React, { useState } from 'react';
import { setUserSession } from '../Utils/common'


const Login = (props) => {

    const username = useFormInput('');
    const password = useFormInput('');

    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);

    const handleLogin = () => {
        setError(null);
        setLoading(true);

        fetch('/api/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username.value,
                password: password.value
            })
        })
            .then((res) => res.json())
            .then((result) => {
                if (!result.success) {
                    setLoading(false);
                    setError(result.message);   
                } else {
                    setLoading(false);
                    console.log("response" + result)
                    setUserSession(result.token, result.user);
                    props.history.push('/dashboard');
                }
            })

    }

    return (
        <div className="text-white text-center">
            Login User
            <br />
            <br />

            <div>
                Username
                <br />
                <input type="text" {...username} autoComplete="new-password" />
            </div>
            <div style={{ marginTop: 10 }}>
                Password
                <br />
                <input type="password" {...password} autoComplete="new-password" />
            </div>
            {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
            <input type="button" value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading} />
            <br />
        </div>
    )
}

const useFormInput = initialValue => {
    const [value, setValue] = useState(initialValue);

    const handleChange = e => {
        setValue(e.target.value);
    }

    return {
        value,
        onChange: handleChange
    }
}

export default Login;