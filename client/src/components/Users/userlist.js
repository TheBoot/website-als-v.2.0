import React, { Component } from 'react';

class UsersList extends Component {


    state = {
        userList: [],
    };


    GetAllUsers = () => {
        fetch('/api/users')
            .then(res => res.json())
            .then(res => {
                let userlist = res.map(result => result);
                // console.log(userlist);
                this.setState({ userList: userlist });
            })
    };

    componentDidMount() {
        this.GetAllUsers();
    };

    render() {
        console.log(this.state.userList);
        const items = this.state.userList.map(item => {
            return (
                <tr key={item.id_user}>
                    <td>
                        {/*  <img src={window.location.origin + '/yourPathHere.jpg'} /> */}
                        {/* <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt=""></img> */}
                        <img src={process.env.PUBLIC_URL + '/img/IMG_3058_als.jpg'} alt=""></img>
                        <a href="/detail-user" class="user-link">{item.id_user}</a>
                        <span class="user-subhead">{item.role}</span>
                    </td>
                    <td>{item.name}</td>
                    <td>{item.role}</td>
                    <td styleName="width: 20%;">
                        <a href="/detail-user" className="table-link">
                            <span className="fa-stack">
                                <i className="fa fa-square fa-stack-2x"></i>
                                <i className="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <a href="/edit-user" className="table-link">
                            <span className="fa-stack">
                                <i className="fa fa-square fa-stack-2x"></i>
                                <i className="fa fa-pencil fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <a href="/delete-user" className="table-link danger">
                            <span className="fa-stack">
                                <i className="fa fa-square fa-stack-2x"></i>
                                <i className="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </td>
                </tr>
            )

            // return (
            //     <tr key={item.id_user}>
            //         <th scope="row">{item.id_user}</th>
            //         <td>{item.name}</td>
            //         <td>{item.role}</td>
            //     </tr>
            // )
        })

        return (
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="main-box clearfix">
                            <div className="table-responsive">
                                <table className="table user-list">
                                    <thead className="text-white">
                                        <tr>
                                            <th><span>User</span></th>
                                            <th><span>Name</span></th>
                                            <th><span>Role</span></th>
                                            <th><span>Edit</span></th>
                                        </tr>
                                    </thead>
                                    <tbody className="text-white">
                                        {items}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )

        // return (
        //     <Table responsive hover>
        //         <thead>
        //             <tr>
        //                 <th className="text-white">ID</th>
        //                 <th className="text-white"> Name</th>
        //                 <th className="text-white">Role</th>
        //             </tr>
        //         </thead>
        //         <tbody className="text-white">
        //             {items}
        //         </tbody>
        //         <Register />
        //     </Table>
        // )
    }



}

export default UsersList;