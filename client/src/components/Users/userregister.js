import React, { Component } from 'react';

class UserRegister extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            name: '',
            password: '',
            email: '',
            address: '',
            phone: '',
            gender: '',
            role: '',
            created_by: ''
        }

        this.username = this.username.bind(this);
        this.name = this.name.bind(this);
        this.email = this.email.bind(this);
        this.address = this.address.bind(this);
        this.phone = this.phone.bind(this);
        this.gender = this.gender.bind(this);
        this.role = this.role.bind(this);
        this.created_by = this.created_by.bind(this);
    }

    username(event) {
        this.setState({ username: event.target.value })
    }

    password(event) {
        this.setState({ password: event.target.value })
    }

    name(event) {
        this.setState({ name: event.target.value })
    }

    email(event) {
        this.setState({ email: event.target.value })
    }

    address(event) {
        this.setState({ address: event.target.value })
    }

    phone(event) {
        this.setState({ phone: event.target.value })
    }

    gender(event) {
        this.setState({ gender: event.target.value })
    }

    role(event) {
        this.setState({ role: event.target.value })
    }

    created_by(event) {
        this.setState({ created_by: event.target.value })
    }

    register(event) {
        fetch('/api/users', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id_user: this.state.username,
                password: this.state.password,
                name: this.state.name,
                email: this.state.email,
                address: this.state.address,
                phone: this.state.phone,
                gender: this.state.gender,
                role: this.state.role,
                created_by: this.state.created_by // get from user admin props
            })
        }).then((res) => res.json())
            .then((result) => {
                if (result && result.success) {
                    this.props.history.push('/user-management');
                }
                else {
                    alert(result.message);
                }
            })
    }

    render() {
        return (
            <div class="container">
            <div class="Back">
                 <i class="fa fa-arrow-left" onclick="Back()"></i>
             </div>
             <p class="h2 text-center">Form</p>
             <form action="" method="post">
                 <div class="preview text-center">
                     <img class="preview-img" src="http://simpleicon.com/wp-content/uploads/account.png" alt="Preview Image" width="200" height="200"/>
                     <div class="browse-button">
                         <i class="fa fa-pencil-alt"></i>
                         <input class="browse-input" type="file" required name="UploadedFile" id="UploadedFile"/>
                     </div>
                     <span class="Error"></span>
                 </div>
                 <div class="form-group">
                     <label>Full Name:</label>
                     <input class="form-control" type="text" name="fullname" required placeholder="Enter Your Full Name"/>
                     <span class="Error"></span>
                 </div>
                 <div class="form-group">
                     <label>Email:</label>
                     <input class="form-control" type="email" name="email" required placeholder="Enter Your Email"/>
                     <span class="Error"></span>
                 </div>
                 <div class="form-group">
                     <label>Password:</label>
                     <input class="form-control" type="password" name="password" required placeholder="Enter Password"/>
                     <span class="Error"></span>
                 </div>
                 <div class="form-group">
                     <label>Gender:</label><br/>
                     <label><input type="radio" name="gender" required value="Male" checked /> Male</label>
                     <label><input type="radio" name="gender" required value="Female" /> Female</label>
                     <label><input type="radio" name="gender" required value="Other" /> Other</label>
                     <span class="Error"></span>
                 </div>
                 <div class="form-group">
                     <input class="btn btn-primary btn-block" type="submit" value="Submit"/>
                 </div>
             </form>
         </div>
        )
    }

}

export default UserRegister;