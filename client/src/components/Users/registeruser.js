import React, { Component } from 'react';
import { 
    Button, 
    Card, 
    CardBody,  
    Col, 
    Container, 
    Form, 
    Input, 
    InputGroup, 
    Row } from 'reactstrap';

class Register extends Component {
    constructor(){
        super();

        this.state = {
            username: '',
            name: '',
            password: '',
            email: '',
            address: '',
            phone: '',
            gender: '',
            role: '',
            created_by: ''
        }

        this.username = this.username.bind(this);
        this.password = this.password.bind(this);
        this.name = this.name.bind(this);
        this.email = this.email.bind(this);
        this.address =this.address.bind(this);
        this.phone = this.phone.bind(this);
        this.gender = this.gender.bind(this);
        this.role = this.role.bind(this);
        this.created_by = this.created_by.bind(this);
        this.register = this.register.bind(this);
    }

    username(event){
        this.setState({username: event.target.value})
    }

    password(event){
        this.setState({password: event.target.value})
    }

    name(event){
        this.setState({name: event.target.value})
    }

    email(event){
        this.setState({email: event.target.value})
    }

    address(event){
        this.setState({address: event.target.value})
    }

    phone(event){
        this.setState({phone: event.target.value})
    }

    gender(event){
        this.setState({gender: event.target.value})
    }

    role(event){
        this.setState({role: event.target.value})
    }

    created_by(event){
        this.setState({created_by: event.target.value})
    }

    register(event){
        fetch('/api/users', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id_user: this.state.username,
                password: this.state.password,
                name: this.state.name,
                email: this.state.email,
                address: this.state.address,
                phone: this.state.phone,
                gender: this.state.gender,
                role: this.state.role,
                created_by: this.state.created_by // get from user admin props
            })
        }).then((res) => res.json())
        .then((result) => {
            if (result && result.success){
                this.props.history.push('/user-management');
            }
            else {
                alert(result.message);
            }
        })
    }

    render() {
        return (
            <div className="user-management align-items-center">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="9" lg="7" xl="6">
                            <Card className="mx-4">
                                <CardBody className="p-4">
                                    <Form>
                                        <div className="row pageheading mb2">
                                            <div className="col-sm-12 btn btn-primary">
                                                Sign Up
                                            </div>
                                        </div>
                                        <InputGroup className="mb-3">
                                            <Input type="text" onChange={this.username} placeholder="Enter Username"/>
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <Input type="text" onChange={this.email} placeholder="Enter Email"/>
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <Input type="password" onChange={this.password} placeholder="Enter Password"/>
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <Input type="text" onChange={this.name} placeholder="Enter Name"/>
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <Input type="text" onChange={this.address} placeholder="Enter address"/>
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <Input type="text" onChange={this.phone} placeholder="Enter Phone"/>
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <Input type="text" onChange={this.gender} placeholder="Enter Gender"/>
                                        </InputGroup>
                                        <InputGroup className="mb-3">
                                            <Input type="text" onChange={this.role} placeholder="Select Role"/>
                                        </InputGroup>
                                        <Button onClick={this.register} color="success" block>Create Account</Button>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }

}

export default Register;