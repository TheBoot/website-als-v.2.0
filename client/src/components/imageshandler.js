import React, { Component } from 'react';
import { Container } from 'reactstrap';



class ImageHandler extends Component {
    state = {
        // Get From database t_images;
        img: [
            "http://strapkulit.com/public/img/IMG_0409_ALS.jpg",
            "http://strapkulit.com/public/img/IMG_0307_toped.jpg",
            "http://strapkulit.com/public/img/IMG_0381_toped.jpg"
        ]
    }

    render() {
        let num = Math.floor(Math.random() * Math.floor(2))

        return (
            <Container>
                <div className="imageHeader">
                    <img src={this.state.img[num]} alt="header.img" />
                </div>
            </Container>
        )
    }

}

export default ImageHandler